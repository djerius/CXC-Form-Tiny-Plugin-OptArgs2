package CXC::Form::Tiny::Plugin::OptArgs2::Class;

# ABSTRACT: Class role for OptArgs2

use v5.20;

use warnings;

our $VERSION = '0.13';

use Hash::Fold ();

use Moo::Role;
use experimental 'signatures', 'postderef';

use namespace::clean;


=method optargs

  \@optargs = $form->optargs;

Return an C<OptArgs2> compatible specification for the form fields

=cut

sub optargs ( $self ) {
    return $self->form_meta->optargs;
}

=method set_input_from_optargs

   $form->set_input_from_optargs( \%optargs );

Set the input form data from the output of L<OptArgs2>'s C<optargs> or
C<class_optargs> functions.

=cut

sub set_input_from_optargs ( $self, $optargs ) {
    # inflate the flat hash into the nested structure and set the
    # form's input
    $self->set_input( $self->inflate_optargs( $optargs ) );
}

=method inflate_optargs

  \%options = $self->inflate( \%optargs );

Inflate the "flat" options hash returned by L<OptArgs2> into the full
hash required to initialize the form.  See
L<CXC::Form::Tiny::Plugin::OptArgs2::Meta/inflate_optargs> for more
information.

=cut

sub inflate_optargs ( $self, $optargs ) {
    return $self->form_meta->inflate_optargs( $optargs );
}


# COPYRIGHT

1;

__END__

=pod

=for stopwords
optargs

=head1 DESCRIPTION

This role is applied to the L<Form::Tiny> class by the
L<CXC::Form::Tiny::Plugin::OptArgs2> plugin.

It provides methods which allow the form to access the options defined
by the DSL commands C<option> and C<argument> as well as to set the
form data from the output of the L<OptArgs2> C<optargs> or
C<class_optargs>.

=cut
